// This program implements complex numbers as an abstract data type
// that supports the standard complex binary operations of addition,
// subtraction, multiplication, and division.

#include <iostream>
using namespace std;

class Complex {
		double real;
		double imag;

	public:
		// constructors
		Complex(): real(0),imag(0){} // default constructor
		Complex(double); // only real part given
		Complex(double, double);  // both real and imaginary parts given

		// other methods
		void writeoutput() {
			if (imag >= 0)
				write();      // write the sum
			else
				negwrite();
		}

		void write() {
			cout << "\n\n\t";
			cout << "The Complex sum is:  " << real <<
			     " + " << imag << "i\n" << endl;
		}

		void negwrite() {
			cout << "\n\n\t";
			cout << "The Complex sum is:  " << real <<
			     " - " <<(-1)*imag << "i" << endl;
		}

		// operator methods
		Complex operator+(const Complex) const;
		Complex operator-(const Complex) const;
		Complex operator*(const Complex) const;
		Complex operator/(const Complex) const;
		Complex operator+=(Complex);
		Complex operator!();
};

// default constructor
//Complex::Complex() {
//	real = imag = 0.0;
//}

// constructor --real given but not imag
Complex::Complex(double r) {
	real = r;
	imag = 0.0;
}

// constructor -- real and imag given
Complex::Complex(double r, double i) {
	real = r;
	imag = i;
}

Complex Complex::operator!() {
	imag = (-1)*imag;
	return Complex(real, imag);
}
// Complex + as a binary operator, returns a Complex value
Complex Complex::operator+(const Complex c) const {
	cout << "The value of real is: " << real << endl;
	cout << "\tThe value of c.real is: " << c.real << endl;
	return Complex(real + c.real, imag + c.imag);
}

// Complex - as a binary operator
Complex Complex::operator-(const Complex c) const {
	return Complex(real - c.real, imag - c.imag);
}

Complex Complex::operator+=(Complex c) { //cannot use const here since the
	// object is being modified
	real = real + c.real;
	imag = imag + c.imag;
	return Complex(real, imag);
}
// Complex * as a binary operator
Complex Complex::operator*(const Complex c) const {
	return Complex(real * c.real - imag * c.imag,
	               imag * c.real + real * c.imag);
}

Complex Complex::operator/(const Complex c) const {
	double abs_sq = c.real * c.real + c.imag * c.imag;

	return Complex((real * c.real + imag * c.imag)/abs_sq,
	               (imag * c.real - real * c.imag)/abs_sq);
}

int main() {
	int a=50;
	int b=100;
	cout<<"\n\tInput the real part: ";
	cin>>a;
	cout<<"\tInput the Imaginary part: ";
	cin>>b;

	Complex c1(a, b);
	Complex c2(2.2, 4.3);
	Complex c4(10); //real number only
	Complex c3;

//   c1 = c1+c2;
//	c1.operator!();   //.operator!();
//	c1.writeoutput();
// + for Complex variables
//   c3 += c1 ;
	c3= c1+c3;
//	c3= c1+c4+c2;
//	c3+=c4;
	c3.writeoutput();

//	c4.writeoutput();

	return (0);
}
